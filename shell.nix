with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "shippy";

  buildInputs = [
    go_1_13.all

    protobuf
  ];

  shellHook = ''
     export GOPATH=/home/mukund/gocode/
     export PATH=$PATH:$GOPATH/bin
  '';

}
