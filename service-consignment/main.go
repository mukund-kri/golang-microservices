package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "mukund.net.in/shippy/service-consignment/proto/consignment"
)

const (
	port = ":50051"
)

type Repository interface {
	Create(*pb.Consignment) (*pb.Consignment, error)
	GetAll() []*pb.Consignment
}

type ConsignmentRepository struct {
	mu           sync.RWMutex
	consignments []*pb.Consignment
}

// Create a new consignment
func (repo *ConsignmentRepository) Create(consignment *pb.Consignment) (*pb.Consignment, error) {
	repo.mu.Lock()
	updated := append(repo.consignments, consignment)
	repo.consignments = updated
	repo.mu.Unlock()
	fmt.Println(consignment)
	return consignment, nil
}

// Return all consignments in the repo
func (repo *ConsignmentRepository) GetAll() []*pb.Consignment {
	return repo.consignments
}

type Service struct {
	repo *ConsignmentRepository
}

func (s *Service) CreateConsignment(ctx context.Context, req *pb.Consignment) (*pb.Response, error) {
	consignment, err := s.repo.Create(req)
	if err != nil {
		return nil, err
	}

	return &pb.Response{Created: true, Consignment: consignment}, nil
}

func (s *Service) GetConsignments(ctx context.Context, req *pb.GetRequest) (*pb.Response, error) {
	return &pb.Response{Consignments: s.repo.GetAll()}, nil
}

func main() {
	fmt.Println("Starting Shippy Service")

	repo := &ConsignmentRepository{}

	// set up our new grpc server
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	// initialize the service
	s := grpc.NewServer()
	pb.RegisterShippingServiceServer(s, &Service{repo})

	reflection.Register(s)

	log.Println("Running on port:", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
