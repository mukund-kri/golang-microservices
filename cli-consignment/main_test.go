package main

import (
	"fmt"
	"testing"
)

func TestParseFile(t *testing.T) {
	fmt.Println("First test")

	con, err := ParseFile("consignment.json")

	if err != nil {
		t.Error("Could not open file")
	}

	if con.VesselId != "vessel001" {
		t.Error("File not correctly deserialized")
	}

}
