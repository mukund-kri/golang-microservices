package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"google.golang.org/grpc"

	pb "mukund.net.in/shippy/service-consignment/proto/consignment"
)

const (
	address     = ":50051"
	defaultName = "consignment.json"
)

func ParseFile(file string) (*pb.Consignment, error) {

	var consignment pb.Consignment

	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &consignment)
	return &consignment, nil
}

func main() {
	fmt.Println("Simple grpc client")

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewShippingServiceClient(conn)

	file := defaultName
	consignment, err := ParseFile(file)
	if err != nil {
		log.Fatalf("Could not open file")
	}
	fmt.Println(consignment)

	r, err := client.CreateConsignment(context.Background(), consignment)
	if err != nil {
		log.Fatalf("Could not greet: %v", err)
	}
	log.Printf("Created: %t", r.Created)

	cs, err := client.GetConsignments(context.Background(), &pb.GetRequest{})
	if err != nil {
		log.Fatalf("Could not retrive consignments %v", err)
	}

	for _, c := range cs.Consignments {
		log.Println(c)
	}
}
